# Install script for directory: /home/jace/cosmos/source/core/libraries/thirdparty/zlib

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local/cosmos/uhabs5")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Default/libraries/zlib/liblocalzlib.a")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/thirdparty/zlib" TYPE FILE FILES
    "/home/jace/cosmos/source/core/libraries/thirdparty/zlib/zlib.h"
    "/home/jace/cosmos/source/core/libraries/thirdparty/zlib/deflate.h"
    "/home/jace/cosmos/source/core/libraries/thirdparty/zlib/crc32.h"
    "/home/jace/cosmos/source/core/libraries/thirdparty/zlib/inffixed.h"
    "/home/jace/cosmos/source/core/libraries/thirdparty/zlib/trees.h"
    "/home/jace/cosmos/source/core/libraries/thirdparty/zlib/zconf.h"
    "/home/jace/cosmos/source/core/libraries/thirdparty/zlib/gzguts.h"
    "/home/jace/cosmos/source/core/libraries/thirdparty/zlib/zutil.h"
    "/home/jace/cosmos/source/core/libraries/thirdparty/zlib/inftrees.h"
    "/home/jace/cosmos/source/core/libraries/thirdparty/zlib/inffast.h"
    "/home/jace/cosmos/source/core/libraries/thirdparty/zlib/inflate.h"
    )
endif()


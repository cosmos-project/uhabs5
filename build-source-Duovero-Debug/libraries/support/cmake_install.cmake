# Install script for directory: /home/jace/cosmos/source/core/libraries/support

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local/cosmos/uhabs5")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/support/libCosmosSupport.a")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/support" TYPE FILE FILES
    "/home/jace/cosmos/source/core/libraries/support/configCosmos.h"
    "/home/jace/cosmos/source/core/libraries/support/cosmos-defs.h"
    "/home/jace/cosmos/source/core/libraries/support/convertdef.h"
    "/home/jace/cosmos/source/core/libraries/support/demlib.h"
    "/home/jace/cosmos/source/core/libraries/support/jpleph.h"
    "/home/jace/cosmos/source/core/libraries/support/socketlib.h"
    "/home/jace/cosmos/source/core/libraries/support/estimation_lib.h"
    "/home/jace/cosmos/source/core/libraries/support/timeutils.h"
    "/home/jace/cosmos/source/core/libraries/support/event.h"
    "/home/jace/cosmos/source/core/libraries/support/command_queue.h"
    "/home/jace/cosmos/source/core/libraries/support/sliplib.h"
    "/home/jace/cosmos/source/core/libraries/support/objlib.h"
    "/home/jace/cosmos/source/core/libraries/support/cosmos-errno.h"
    "/home/jace/cosmos/source/core/libraries/support/convertlib.h"
    "/home/jace/cosmos/source/core/libraries/support/ephemlib.h"
    "/home/jace/cosmos/source/core/libraries/support/jsondef.h"
    "/home/jace/cosmos/source/core/libraries/support/nrlmsise-00.h"
    "/home/jace/cosmos/source/core/libraries/support/transferlib.h"
    "/home/jace/cosmos/source/core/libraries/support/jsonlib.h"
    "/home/jace/cosmos/source/core/libraries/support/datadef.h"
    "/home/jace/cosmos/source/core/libraries/support/elapsedtime.h"
    "/home/jace/cosmos/source/core/libraries/support/datalib.h"
    "/home/jace/cosmos/source/core/libraries/support/print_utils.h"
    "/home/jace/cosmos/source/core/libraries/support/stringlib.h"
    "/home/jace/cosmos/source/core/libraries/support/timelib.h"
    "/home/jace/cosmos/source/core/libraries/support/geomag.h"
    )
endif()


# Install script for directory: /home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local/cosmos/uhabs5")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/mavlink" TYPE FILE FILES
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_rc_channels_raw.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_local_position_ned_cov.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_position_target_local_ned.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_named_value_float.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_param_request_list.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_storage_information.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_global_position_int_cov.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_change_operator_control.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_follow_target.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_log_request_data.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_rc_channels_scaled.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_message_interval.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mission_item_reached.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_gps_status.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_att_pos_mocap.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_terrain_request.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_radio_status.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_command_ack.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_local_position_ned_system_global_offset.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_statustext.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mission_item_int.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_wind_cov.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_button_change.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_set_gps_global_origin.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_vfr_hud.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/message.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_vision_position_estimate.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_command_int.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_log_entry.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_hil_state_quaternion.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_collision.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_attitude_target.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_local_position_ned.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mission_write_partial_list.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mission_clear_all.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_hil_rc_inputs_raw.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_manual_setpoint.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_distance_sensor.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_global_position_int.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mission_ack.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_camera_trigger.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_gps_global_origin.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mission_current.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_landing_target.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_hil_actuator_controls.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_resource_request.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_highres_imu.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_rc_channels.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_timesync.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_auth_key.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_log_request_list.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_data_transmission_handshake.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_log_erase.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_autopilot_version.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_sys_status.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_terrain_check.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_control_system_state.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_camera_image_captured.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mission_request_partial_list.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_scaled_pressure.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_attitude.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/gtestsuite.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_hil_controls.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_command_long.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_gps2_raw.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_home_position.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_global_vision_position_estimate.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_gps_input.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_hil_sensor.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_system_time.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_log_data.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_v2_extension.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_raw_imu.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mount_orientation.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mission_item.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_scaled_imu.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_hil_state.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_param_set.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_logging_data.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mission_set_current.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_scaled_imu3.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_gps2_rtk.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_altitude.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_optical_flow_rad.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_manual_control.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mission_request_list.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_log_request_end.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_battery_status.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_sim_state.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_set_home_position.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_safety_allowed_area.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_ping.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_scaled_pressure3.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_debug.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_vision_speed_estimate.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_serial_control.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_terrain_data.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_logging_ack.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_camera_capture_status.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_scaled_imu2.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_safety_set_allowed_area.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_hil_optical_flow.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_power_status.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_camera_information.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_data_stream.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/msgmap.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_gps_rtk.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_request_data_stream.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mission_request.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_gps_rtcm_data.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_servo_output_raw.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_actuator_control_target.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_estimator_status.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_set_actuator_control_target.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_debug_vect.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_set_position_target_global_int.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_attitude_quaternion.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_heartbeat.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_param_request_read.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_adsb_vehicle.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_terrain_report.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_hil_gps.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_set_attitude_target.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_gps_raw_int.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_set_position_target_local_ned.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_memory_vect.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_setup_signing.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_change_operator_control_ack.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_param_map_rc.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mission_request_int.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_set_mode.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_raw_pressure.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_vicon_position_estimate.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/common.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_logging_data_acked.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_encapsulated_data.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_optical_flow.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_named_value_int.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_position_target_global_int.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_scaled_pressure2.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_camera_settings.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_high_latency.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_rc_channels_override.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_vibration.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_gps_inject_data.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_mission_count.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_extended_sys_state.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_file_transfer_protocol.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_attitude_quaternion_cov.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_play_tune.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_nav_controller_output.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_flight_information.hpp"
    "/home/jace/cosmos/source/projects/uhabs5/source/libraries/mavlink/mavlink_msg_param_value.hpp"
    )
endif()


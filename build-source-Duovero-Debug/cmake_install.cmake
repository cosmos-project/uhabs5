# Install script for directory: /home/jace/cosmos/source/projects/uhabs5/source

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local/cosmos/uhabs5")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/agent/cmake_install.cmake")
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/support/cmake_install.cmake")
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/math/cmake_install.cmake")
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/physics/cmake_install.cmake")
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/png/cmake_install.cmake")
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/jpeg/cmake_install.cmake")
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/zlib/cmake_install.cmake")
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/device/general/cmake_install.cmake")
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/device/disk/cmake_install.cmake")
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/device/cpu/cmake_install.cmake")
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/device/i2c/cmake_install.cmake")
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/device/serial/cmake_install.cmake")
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/libraries/cmake_install.cmake")
  include("/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/programs/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/jace/cosmos/source/projects/uhabs5/build-source-Duovero-Debug/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")

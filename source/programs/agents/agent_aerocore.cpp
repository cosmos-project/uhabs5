//COSMOS libraries
#include "support/configCosmos.h"
#include "support/elapsedtime.h"
#include "support/timeutils.h"
#include "device/serial/serialclass.h"
#include "agent/agentclass.h"

//mavlink library
#include <mavlink.h>

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int myagent();

string nodename = "uhabs_5";
string agentname = "aerocore";

int waitsec = 1; // wait to find other agents of your 'type/name', seconds
int loopmsec = 1; // period of heartbeat
char buf4[512];

beatstruc beat_agent_002;
Agent *agent; // to access the cosmos data, will change later

#define MAXBUFFERSIZE 256 // comm buffer for agents

int main(int argc, char *argv[])
{
  cout << "Starting agent " << endl;

  //Create agent
  agent = new Agent(nodename, agentname);

  //  string sohtemp = json_list_of_soh(agent->cinfo).c_str();
  //  printf("Devices: %s\n", sohtemp.c_str());

  // Set SOH String
  char agent_aerocore_soh[2000] = "{\"device_tsen_temp_000\","
                                  "\"device_imu_accel_000\","
                                  "\"device_imu_utc_000\","
                                  "\"device_gps_geods_000\","
                                  "\"device_psen_press_000\","
                                  "\"device_gps_heading_000\"}";

  //                                  "\"device_ssen_elevation_000\","
  //                                  "\"device_psen_press_000\","

  agent->set_sohstring(agent_aerocore_soh);

  myagent();

  return 0;
}

int myagent()
{
  cout << "agent " << agentname <<  " ...online " << endl;

  //Initialize the required buffers
  mavlink_message_t msg;
  mavlink_status_t status;
  //uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  //uint16_t len;
  //int bytes_sent;

  // Example variable, by declaring them static they're persistent
  // and will thus track the system state
  static int packet_drops = 0;
  //static int mode = 0; //Defined in mavlink_types.h, which is included by mavlink.h

  cout << "Open serial port /dev/ttyO1, status: " ;
  Serial serial("/dev/ttyO1", 115200, 8, 0, 1);
  cout << serial.get_error() << endl;

  //declare variables
  float baro;
  float alti;
  float tin;
  float xacc;
  float yacc;
  float zacc;
  float e7 = 10000000;
  uint64_t time;
  int32_t lat;
  int32_t lon;
  uint16_t vel;
  int16_t head;

  ElapsedTime et;
  et.start();

  double timetag = 0;

  while(agent->running())
    {
      uint8_t c = serial.get_char();
      //cout << c << endl;
      //Try to get a new message
      if(mavlink_parse_char(MAVLINK_COMM_1, c, &msg, &status))
        {
          // Handle message
          printf("Received packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d", msg.sysid, msg.compid, msg.len, msg.msgid);
          cout << endl;
          // 74 - VRF_HUD
          // 87 - POSITION_TARGET_GLOBAL_INT
          // 0 - HEARTBEAT
          // 1 - SYS_STATUS
          // 105 - HIGHRES_IMU

          switch(msg.msgid)
            {
            case MAVLINK_MSG_ID_HIGHRES_IMU:
              mavlink_highres_imu_t imu;
              mavlink_msg_highres_imu_decode(&msg, &imu);
              //                printf("Got message HIGHRES_IMU");
              //                printf("\t time: %llu\n", imu.time_usec);
              //                printf("\t acc  (NED):\t% f\t% f\t% f (m/s^2)\n", imu.xacc, imu.yacc, imu.zacc);
              //                printf("\t gyro (NED):\t% f\t% f\t% f (rad/s)\n", imu.xgyro, imu.ygyro, imu.zgyro);
              //                printf("\t mag  (NED):\t% f\t% f\t% f (Ga)\n", imu.xmag, imu.ymag, imu.zmag);
              //                printf("\t baro: \t %f (mBar)\n", imu.abs_pressure);
              //                printf("\t altitude: \t %f (m)\n", imu.pressure_alt);
              //                printf("\t temperature: \t %f C\n", imu.temperature);
              //                printf("\n");
              //                cout << endl;
              time = imu.time_usec;
              baro = imu.abs_pressure;
              //              alti = imu.pressure_alt;
              tin = imu.temperature;
              xacc = imu.xacc;
              yacc = imu.yacc;
              zacc = imu.zacc;
              break;

            case MAVLINK_MSG_ID_GPS_RAW_INT:
              mavlink_gps_raw_int_t pos;
              mavlink_msg_gps_raw_int_decode(&msg,&pos);
              //              printf("Got message POSITION\n");
              //              printf("\t time: %llu\n", imu.time_usec);
              //              printf("\t lat:\t %d\n", pos.lat_int);
              //              printf("\t lon:\t %d\n", pos.lon_int);
              //              printf("\t alt:\t %d\n", pos.alt);
              //              printf("\n");
              lat = pos.lat / e7;
              lon = pos.lon /e7;
              //              alti = pos.alt;
              vel = pos.vel;
              break;

            case MAVLINK_MSG_ID_VFR_HUD:
              mavlink_vfr_hud_t vfr;
              mavlink_msg_vfr_hud_decode(&msg,&vfr);
              head = vfr.heading;
              alti = vfr.alt;
              printf("heading: %f\n",head);
              break;

            default:
              //Do nothing
              break;
            }
        }

      //assign value
      agent->cinfo->devspec.imu[0]->utc = time;
      agent->cinfo->devspec.tsen[0]->temp = tin;
      agent->cinfo->devspec.gps[0]->geods.h = alti;
      //      agent->cinfo->devspec.ssen[0]->elevation = alti;
      agent->cinfo->devspec.gps[0]->geods.lat = lat;
      agent->cinfo->devspec.gps[0]->geods.lon = lon;
      agent->cinfo->devspec.psen[0]->press = baro;
      agent->cinfo->devspec.imu[0]->accel.col[0] = xacc;
      agent->cinfo->devspec.imu[0]->accel.col[1] = yacc;
      agent->cinfo->devspec.imu[0]->accel.col[2] = zacc;
      agent->cinfo->devspec.gps[0]->heading = head;

      //        cout << tin << endl;
      //And get the next one
      //COSMOS_SLEEP(1);

//            if (et.split() < 10) {
//                //save to txt
//                timetag = currentmjd();
//                ofstream myfile ("cosmos/nodes/uhabs5/outgoing/soh/soh"+to_string(timetag)+".txt");
//                if (myfile.is_open()) {
//                    myfile << "utc," << time;
//                    myfile << ",temp," << tin;
//                    myfile << ",press," << baro;
//                    myfile << ",alt," << alti;
//                    myfile << ",lat," << lat;
//                    myfile << ",lon," << lon;
//                    myfile << ",xacc," << xacc;
//                    myfile << ",yacc," << yacc;
//                    myfile << ",zacc," << zacc;
//                    myfile << ",vel," << vel;
//                    myfile << ",head," << head;
//                    myfile << "\n";
//                  }
//              } else {
//                myfile.close();
//                et.reset();
//                timetag = currentmjd();
//              }
    }
  //Update global packet drops counter
  packet_drops += status.packet_rx_drop_count;

  return (0);

}

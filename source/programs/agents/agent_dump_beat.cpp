/********************************************************************
* Copyright (C) 2015 by Interstel Technologies, Inc.
*   and Hawaii Space Flight Laboratory.
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.com>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

/*! \file agent.cpp
* \brief Agent control program source
*/

//! \ingroup general
//! \defgroup agent_client Agent control program
//! This program allows communication with any of the Agents on the local network.
//! With it you can:
//! - list available Agents
//! - acquire the request list of specific Agents
//! - command specific Agents
//! - monitor Agent traffic

#include "support/configCosmos.h"
#include <stdlib.h>
#include "agent/agentclass.h"
#include "support/jsonlib.h"
#include "physics/physicslib.h"
#include "support/datalib.h"
#include "sys/stat.h"
#include "limits.h"
#include <iostream>

const int REQUEST_WAIT_TIME = 2;
const int SERVER_WAIT_TIME = 6;

std::string output;
string node_name = "uhabs5";
string agent_name = "";


int main(int argc, char *argv[])
{
    int nbytes;
    beatstruc cbeat;
    std::vector<std::string> nl;
    data_list_nodes(nl);
    Agent *agent;

    // dont' print debug messages
    //agent->debug_level = 0;
    agent = new Agent("","",1,AGENTMAXBUFFER,false,0,NetworkType::UDP,0);

    double lmjd = 0., dmjd;
    std::string channel;
    Agent::AgentMessage cnum;
    Agent::messstruc message;
    int i;
    locstruc loc;

    channel = argv[2];

    cnum = Agent::AgentMessage::BEAT;

    while (1)
    {
        int32_t iretn;
        if ((iretn=agent->readring(message, cnum, 1., Agent::Where::TAIL)) > 0)
        {
            Agent::AgentMessage pretn = (Agent::AgentMessage)iretn;

            // Skip if either not Agent::AgentMessage::ALL, or not desired AGENT_MESSAGE
            if (!channel.empty() && cnum != pretn)
            {
                continue;
            }

            if (!node_name.empty() && node_name != message.meta.beat.node)
            {
                continue;
            }

            if (!agent_name.empty() && agent_name != message.meta.beat.proc)
            {
                continue;
            }

            switch (pretn)
            {
            case Agent::AgentMessage::SOH:
                printf("[SOH]");
                break;
            case Agent::AgentMessage::BEAT:
                printf("[BEAT]");
                break;
            default:
                printf("[%d]",pretn);
                break;
            }

            printf("%.15g:[%s:%s][%s:%u](%" PRIu64 ":%" PRIu64 ":%" PRIu64 ")\n",message.meta.beat.utc, message.meta.beat.node, message.meta.beat.proc, message.meta.beat.addr, message.meta.beat.port, message.jdata.size(), message.adata.size(), message.bdata.size());
            printf("%s\n",message.jdata.c_str());
            if (pretn < Agent::AgentMessage::BINARY)
            {
                if (!channel.empty())
                {
                    printf("%s\n",message.adata.c_str());
                }
            }

            if ((channel=="info") && pretn == Agent::AgentMessage::TRACK)
            {
                if (agent->cinfo->node.loc.utc > 0.)
                {
                    if (lmjd > 0.)
                        dmjd = 86400.*(agent->cinfo->node.loc.utc-lmjd);
                    else
                        dmjd = 0.;
                    loc.pos.icrf.s = agent->cinfo->node.loc.pos.icrf.s;
                    loc.pos.utc = agent->cinfo->node.loc.utc;
                    pos_eci(&loc);
                    printf("%16.15g %6.4g %s %8.3f %8.3f %8.3f %5.1f %5.1f %5.1f\n",agent->cinfo->node.loc.utc,dmjd,agent->cinfo->node.name,DEGOF(loc.pos.geod.s.lon),DEGOF(loc.pos.geod.s.lat),loc.pos.geod.s.h,agent->cinfo->node.powgen,agent->cinfo->node.powuse,agent->cinfo->node.battlev);
                    lmjd = agent->cinfo->node.loc.utc;
                }
            }

            if ((channel=="imu") && pretn == Agent::AgentMessage::IMU)
            {
                for (i=0; i<agent->cinfo->devspec.imu_cnt; i++)
                {
                    if (agent->cinfo->agent[0].beat.utc > 0.)
                    {
                        if (lmjd > 0.)
                            dmjd = 86400.*(agent->cinfo->agent[0].beat.utc-lmjd);
                        else
                            dmjd = 0.;
                        printf("%.15g %.4g\n",loc.utc,dmjd);
                        lmjd = agent->cinfo->agent[0].beat.utc;
                    }
                }
            }
        }
        fflush(stdout);
    } //end infinite while loop

    return 0;
}

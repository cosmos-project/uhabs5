# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/jace/cosmos/source/projects/uhabs5/source/programs/agents/agent_aerocore.cpp" "/home/jace/cosmos/source/projects/uhabs5/source/build/arm/programs/agents/CMakeFiles/agent_aerocore.dir/agent_aerocore.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/jace/cosmos/source/core/libraries"
  "/home/jace/cosmos/source/core/libraries/thirdparty"
  "/home/jace/cosmos/source/core/libraries/device"
  "/home/jace/cosmos/source/core/libraries/device/general"
  "/home/jace/cosmos/source/core/libraries/device/disk"
  "/home/jace/cosmos/source/core/libraries/device/cpu"
  "/home/jace/cosmos/source/core/libraries/device/i2c"
  "/home/jace/cosmos/source/core/libraries/device/serial"
  "/usr/local/cosmos/src/core/libraries"
  "../../libraries"
  "../../libraries/mavlink"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/jace/cosmos/source/projects/uhabs5/source/build/arm/libraries/device/serial/CMakeFiles/CosmosDeviceSerial.dir/DependInfo.cmake"
  "/home/jace/cosmos/source/projects/uhabs5/source/build/arm/libraries/device/cpu/CMakeFiles/CosmosDeviceCpu.dir/DependInfo.cmake"
  "/home/jace/cosmos/source/projects/uhabs5/source/build/arm/libraries/device/disk/CMakeFiles/CosmosDeviceDisk.dir/DependInfo.cmake"
  "/home/jace/cosmos/source/projects/uhabs5/source/build/arm/libraries/device/general/CMakeFiles/CosmosDeviceGeneral.dir/DependInfo.cmake"
  "/home/jace/cosmos/source/projects/uhabs5/source/build/arm/libraries/agent/CMakeFiles/CosmosAgent.dir/DependInfo.cmake"
  "/home/jace/cosmos/source/projects/uhabs5/source/build/arm/libraries/physics/CMakeFiles/CosmosPhysics.dir/DependInfo.cmake"
  "/home/jace/cosmos/source/projects/uhabs5/source/build/arm/libraries/support/CMakeFiles/CosmosSupport.dir/DependInfo.cmake"
  "/home/jace/cosmos/source/projects/uhabs5/source/build/arm/libraries/math/CMakeFiles/CosmosMath.dir/DependInfo.cmake"
  "/home/jace/cosmos/source/projects/uhabs5/source/build/arm/libraries/zlib/CMakeFiles/localzlib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
